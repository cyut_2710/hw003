public class hw3 {
    public static void main(String[] args){
        //設i=2和j=1
        for (int i = 2, j = 1;
             //當i小於10
             j < 10;
             //假如i==9運算結果為true會使用(++j/j)+1讓=2 false的話會使用(i+1) (j=++j，是先對j加1，再將j值（1）存入)
             i = (i==9)?((++j/j)+1):(i+1)){
            //輸出i乘以j等於i*j的數值
            System.out.printf("%d*%d=%2d%c",i,j,i*j,
                    //假如i==9運算結果為true會換行 如果false就繼續乘
                    (i==9 ? '\n' : ' '));
        }
    }
}
